package ru.tsc.ichaplygina.taskmanager.exception.incorrect;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public final class IndexIncorrectException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Error! Incorrect index: ";

    public IndexIncorrectException(final long index) {
        super(MESSAGE + index);
    }

}
