package ru.tsc.ichaplygina.taskmanager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.entity.IWBS;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.EMPTY;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractBusinessEntity extends AbstractModel implements IWBS {

    @NotNull
    @Column(name = "created_dt")
    private Date created = new Date();

    @Nullable
    @Column(name = "completed_dt")
    private Date dateFinish;

    @Nullable
    @Column(name = "started_dt")
    private Date dateStart;

    @Nullable
    @Column
    private String description;

    @NotNull
    @Column
    private String name;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @NotNull
    @ManyToOne
    private User user;

    public AbstractBusinessEntity(@NotNull final String name, @Nullable final String description, @NotNull final User user) {
        this.name = name;
        this.description = description != null ? description : EMPTY;
        this.user = user;
    }

    public void setStatus(Status status) {
        this.status = status;
        if (status.equals(Status.COMPLETED)) dateFinish = new Timestamp(new Date().getTime());
        if (status.equals(Status.IN_PROGRESS)) dateStart = new Timestamp(new Date().getTime());
    }
}
