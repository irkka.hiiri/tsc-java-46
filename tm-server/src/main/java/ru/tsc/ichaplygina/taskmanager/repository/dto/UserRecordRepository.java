package ru.tsc.ichaplygina.taskmanager.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.repository.dto.IUserRecordRepository;
import ru.tsc.ichaplygina.taskmanager.dto.UserDTO;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRecordRepository extends AbstractRecordRepository<UserDTO> implements IUserRecordRepository {

    public UserRecordRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM UserDTO").executeUpdate();
    }

    @Override
    public @NotNull List<UserDTO> findAll() {
        return entityManager.createQuery("FROM UserDTO", UserDTO.class).getResultList();
    }

    @Override
    public @Nullable UserDTO findByEmail(@NotNull String email) {
        return entityManager
                .createQuery("FROM UserDTO e WHERE e.email = :email", UserDTO.class)
                .setParameter("email", email)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable UserDTO findById(@NotNull String id) {
        return entityManager.find(UserDTO.class, id);
    }

    @Override
    public @Nullable UserDTO findByLogin(@NotNull String login) {
        return entityManager
                .createQuery("FROM UserDTO e WHERE e.login = :login", UserDTO.class)
                .setParameter("login", login)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable String findIdByEmail(@NotNull String email) {
        return entityManager
                .createQuery("SELECT id FROM UserDTO e WHERE e.email = :email", String.class)
                .setParameter("email", email)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable String findIdByLogin(@NotNull String login) {
        return entityManager
                .createQuery("SELECT id FROM UserDTO e WHERE e.login = :login", String.class)
                .setParameter("login", login)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM UserDTO e", Long.class)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public void removeById(@NotNull String id) {
        entityManager.remove(entityManager.getReference(UserDTO.class, id));
    }

    @Override
    public void removeByLogin(@NotNull String login) {
        entityManager.createQuery("DELETE FROM UserDTO e WHERE e.login = :login")
                .setParameter("login", login).executeUpdate();
    }

}
