package ru.tsc.ichaplygina.taskmanager.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.repository.model.IAbstractBusinessEntityRepository;
import ru.tsc.ichaplygina.taskmanager.model.AbstractBusinessEntity;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class AbstractBusinessEntityRepository<E extends AbstractBusinessEntity> extends AbstractRepository<E> implements IAbstractBusinessEntityRepository<E> {


    public AbstractBusinessEntityRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public abstract void clear();

    @Override
    public abstract void clearForUser(@NotNull String currentUserId);

    @NotNull
    @Override
    public abstract List<E> findAll();

    @NotNull
    @Override
    public abstract List<E> findAllForUser(@NotNull String userId);

    @Nullable
    @Override
    public abstract E findById(@NotNull String id);

    @Nullable
    @Override
    public abstract E findByIdForUser(@NotNull String userId, @NotNull String id);

    @Nullable
    @Override
    public abstract E findByIndex(int index);

    @Nullable
    @Override
    public abstract E findByIndexForUser(@NotNull String userId, int index);

    @Nullable
    @Override
    public abstract E findByName(@NotNull String name);

    @Nullable
    @Override
    public abstract E findByNameForUser(@NotNull String userId, @NotNull String name);

    @Nullable
    @Override
    public abstract String getIdByIndex(int index);

    @Nullable
    @Override
    public abstract String getIdByIndexForUser(@NotNull String userId, int index);

    @Nullable
    @Override
    public abstract String getIdByName(@NotNull String name);

    @Nullable
    @Override
    public abstract String getIdByNameForUser(@NotNull String userId, String name);

    @Override
    public abstract long getSize();

    @Override
    public abstract long getSizeForUser(@NotNull String userId);

    @Override
    public abstract void removeById(@NotNull String id);

    @Override
    public abstract void removeByIdForUser(@NotNull String userId, @NotNull String id);

    @Override
    public abstract void removeByIndex(int index);

    @Override
    public abstract void removeByIndexForUser(@NotNull String userId, int index);

    @Override
    public abstract void removeByName(@NotNull String name);

    @Override
    public abstract void removeByNameForUser(@NotNull String userId, @NotNull String name);
}
