package ru.tsc.ichaplygina.taskmanager.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.repository.model.ISessionRepository;
import ru.tsc.ichaplygina.taskmanager.model.Session;

import javax.persistence.EntityManager;
import java.util.List;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM Session e").executeUpdate();
    }

    @Override
    public @NotNull List<Session> findAll() {
        return entityManager.createQuery("FROM Session", Session.class).getResultList();
    }

    @Override
    public @Nullable Session findById(@NotNull final String id) {
        return entityManager.find(Session.class, id);
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(1) FROM Session e", Long.class).getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String id) {
        entityManager.remove(entityManager.getReference(Session.class, id));
    }

}
